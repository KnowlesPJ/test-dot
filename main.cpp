#include <iostream>
#include <numeric>
#include <vector>
#include <molpro/Profiler.h>
#include <Eigen/Dense>
#if __has_include(<mkl.h>)
#include <mkl.h>
#else
#include <cblas.h>
#endif


constexpr size_t n = 1000;
constexpr size_t repeat = 200000000 / n;
int main() {
  std::vector<double> v1(n, 1);
  std::vector<double> v2(n, 1);
  molpro::profiler::Profiler p("dot", true, true);
  {
    auto pp = p.push("load cblas");
    auto load_cblas = cblas_ddot(1, v1.data(), 1, v2.data(), 1);
  }
  double reference_result;
  {
    auto pp = p.push("indexed loop");
    double result = 0;
    p += n * repeat * 2;
    for (size_t rep = 0; rep < repeat; rep++)
      for (size_t i = 0; i < n; i++)
        result += v1[i] * v2[i];
    reference_result = result;
  }
  {
    auto pp = p.push("iterator loop");
    double result = 0;
    p += n * repeat * 2;
    for (size_t rep = 0; rep < repeat; rep++)
      for (auto e1 = v1.begin(); e1 < v1.end(); e1++)
        result += (*e1) * (*(v2.begin() - v1.begin() + e1));
    std::cout << result << " " << reference_result << std::endl;
  }
  {
    auto pp = p.push("std::inner_product");
    double result = 0;
    p += n * repeat * 2;
    for (size_t rep = 0; rep < repeat; rep++)
      result += std::inner_product(v1.begin(), v1.end(), v2.begin(), 0);
    std::cout << result << " " << reference_result << std::endl;
  }
  {
    auto pp = p.push("Eigen3");
    Eigen::Map<Eigen::VectorXd> v1e(v1.data(), v1.size());
    Eigen::Map<Eigen::VectorXd> v2e(v2.data(), v2.size());
    double result = 0;
    p += n * repeat * 2;
    for (size_t rep = 0; rep < repeat; rep++)
      result += v1e.dot(v2e);
    std::cout << result << " " << reference_result << std::endl;
  }
  {
    auto pp = p.push("cblas");
    auto result = 0;
    p += n * repeat * 2;
    for (size_t rep = 0; rep < repeat; rep++)
      result += cblas_ddot(n, v1.data(), 1, v2.data(), 1);
    std::cout << result << " " << reference_result << std::endl;
  }
  std::cout << p.str() << std::endl;
  return 0;
}
